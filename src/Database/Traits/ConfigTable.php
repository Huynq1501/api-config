<?php

namespace nguyenanhung\Backend\Your_Project\Database\Traits;

use Illuminate\Support\Collection;

/**
 * Trait SignatureTable
 *
 * @package   nguyenanhung\Backend\Your_Project\Database\Traits
 * @author    713uk13m <dev@nguyenanhung.com>
 * @copyright 713uk13m <dev@nguyenanhung.com>
 */
trait ConfigTable
{
    /**
     * Function create
     *
     * @param array $data
     * @return array|bool|Collection|mixed|object|string|null
     * @author   : 713uk13m <dev@nguyenanhung.com>
     * @copyright: 713uk13m <dev@nguyenanhung.com>
     * @time     : 22/06/2022 56:41
     */
    public function createConfig(array $data = array())
    {
        // connect to config table
        $table = 'config';
        $DB = $this->connection();
        $DB->setTable($table);

        //create result
        $result = $DB->add($data);
        $DB->disconnect();

        return $result !== 0;
    }

    public function updateConfig(array $data = array())
    {
        // connect to config table
        $table = 'config';
        $DB = $this->connection();
        $DB->setTable($table);

        //update config
        $result = $DB->update($data, $data['id']);
        $DB->disconnect();

        return $result;
    }

    public function checkConfigExists($id): bool
    {
        $table = 'config';
        $DB = $this->connection();
        $DB->setTable($table);

        //create result
        $result = $DB->checkExists($id);
        $DB->disconnect();

        return $result === 1;
    }

    public function listConfig()
    {
        // connect to config table
        $table = 'config';
        $DB = $this->connection();
        $DB->setTable($table);

        //get config data
        $result = $DB->getResult();
        $DB->disconnect();

        return $result;
    }

}
