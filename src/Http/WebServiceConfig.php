<?php

namespace nguyenanhung\Backend\Your_Project\Http;

use nguyenanhung\Classes\Helper\Filter;

/**
 * Class WebServiceConfig
 *
 * @package   nguyenanhung\Backend\Your_Project\Http
 * @author    713uk13m <dev@nguyenanhung.com>
 * @copyright 713uk13m <dev@nguyenanhung.com>
 */
class WebServiceConfig extends BaseHttp
{
    /**
     * WebServiceConfig constructor.
     *
     * @param array $options
     *
     * @author   : 713uk13m <dev@nguyenanhung.com>
     * @copyright: 713uk13m <dev@nguyenanhung.com>
     */
    public function __construct(array $options = array())
    {
        parent::__construct($options);
        $this->logger->setLoggerSubPath(__CLASS__);
    }

    public function createConfig(): WebServiceConfig
    {
        $required = ['id', 'language', 'value', 'label', 'type', 'status'];
        $filter = Filter::filterInputDataIsArray($this->inputData, $required);
        if ($filter === false) {
            $response = array(
                'result' => self::EXIT_CODE['invalidParams'],
                'desc' => 'sai hoặc thiếu tham số',
                'inputData' => $this->inputData
            );
        } else {
            $id = $this->inputData['id'];
            $language = $this->inputData['language'];
            $value = $this->inputData['value'];
            $label = $this->inputData['label'];
            $type = $this->formatTypeConfig($this->inputData);
            $status = $this->formatStatusConfig($this->inputData);

            if ($id === null || $language === null || $value === null || $label === null || $type === null || $status === null) {
                $response = array(
                    'result' => self::EXIT_CODE['paramsIsEmpty'],
                    'desc' => 'Sai hoac thieu tham so.',
                    'inputData' => $this->inputData
                );
            } else {
                $data = array(
                    'id' => $id,
                    'language' => $language,
                    'value' => $value,
                    'label' => $label,
                    'type' => $type,
                    'status' => $status,
                );

                $checkDuplicateId = $this->db->checkConfigExists($data['id']);

                if ($checkDuplicateId) {
                    $response = array(
                        'result' => self::EXIT_CODE['duplicatePrimaryKey'],
                        'desc' => 'Id này đã tồn tại',
                    );
                } else {
                    $this->db->createConfig($data);
                    $response = array(
                        'result' => self::EXIT_CODE['success'],
                        'desc' => 'Đã ghi nhận config thành công',
                        'insert_id' => $data['id'],
                    );
                }
            }
        }

        $this->response = $response;

        return $this;

    }

    public function listConfig(): WebServiceConfig
    {
        $listConfig = $this->db->listConfig();

        $totalRecord = $listConfig->count();
        if ($totalRecord > 0) {
            $response = array(
                'result' => self::EXIT_CODE['success'],
                'desc' => 'Danh sách config',
                'data' => $listConfig,
            );
        } else {
            $response = array(
                'result' => self::EXIT_CODE['success'],
                'desc' => 'Không tồn tại bản ghi',
                'data' => $listConfig,
            );
        }
        $this->response = $response;

        return $this;
    }

    public function updateConfig(): WebServiceConfig
    {
        $required = ['id', 'language', 'value', 'label', 'type', 'status'];
        $filter = Filter::filterInputDataIsArray($this->inputData, $required);
        if ($filter === false) {
            $response = array(
                'result' => self::EXIT_CODE['invalidParams'],
                'desc' => 'sai hoặc thiếu tham số',
                'inputData' => $this->inputData
            );
        } else {
            $id = $this->inputData['id'];
            $language = $this->inputData['language'];
            $value = $this->inputData['value'];
            $label = $this->inputData['label'];
            $type = $this->formatTypeConfig($this->inputData);
            $status = $this->formatStatusConfig($this->inputData);

            if ($id === null || $language === null || $value === null || $label === null || $type === null || $status === null) {
                $response = array(
                    'result' => self::EXIT_CODE['paramsIsEmpty'],
                    'desc' => 'Sai hoac thieu tham so.',
                    'inputData' => $this->inputData
                );
            } else {
                $data = array(
                    'id' => $id,
                    'language' => $language,
                    'value' => $value,
                    'label' => $label,
                    'type' => $type,
                    'status' => $status,
                );
                $checkExistId = $this->db->checkConfigExists($this->inputData['id']);
                if ($checkExistId) {
                    $result = $this->db->updateConfig($data);
                    if ($result) {
                        $response = array(
                            'result' => self::EXIT_CODE['success'],
                            'desc' => 'Đã ghi nhận update config thành công',
                            'update_id' => $data['id'],
                        );
                    } else {
                        $response = array(
                            'result' => self::EXIT_CODE['success'],
                            'desc' => 'Không có gì thay đổi',
                        );
                    }

                } else {
                    $response = array(
                        'result' => self::EXIT_CODE['notFound'],
                        'desc' => 'Id này không tồn tại',
                    );
                }
            }
        }

        $this->response = $response;

        return $this;
    }

}
