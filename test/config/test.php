<?php

use nguyenanhung\Backend\Your_Project\Http\WebServiceConfig;

/**
 * Project template-backend-package
 * Created by PhpStorm
 * User: <huynq2@beetsoft.com.vn>
 * Copyright: <huynq2@beetsoft.com.vn>
 * Date: 07/07/2022
 * Time: 00:19
 */
require_once __DIR__ . '/../../vendor/autoload.php';

$config = [
    'DATABASE' => [
        'driver' => 'mysql',
        'host' => '127.0.0.1',
        'username' => 'root',
        'password' => '150115',
        'database' => 'base_cms_8.0',
        'port' => 3306,
        'prefix' => 'tnv_',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ],
    'OPTIONS' => [
        'showSignature' => true,
        'debugStatus' => true,
        'debugLevel' => 'error',
        'loggerPath' => __DIR__ . '/../tmp/logs/',
        // Cache
        'cachePath' => __DIR__ . '/../tmp/cache/',
        'cacheTtl' => 3600,
        'cacheDriver' => 'files',
        'cacheFileDefaultChmod' => 0777,
        'cacheSecurityKey' => 'BACKEND-SERVICE',
    ]
];

$inputData = [
    'id' => 'hippo19',
    'language' => '1',
    'value' => 31111,
    'label' => 'abcaaa',
    'type' => 2,
    'status' => 3,
];

//api create
//$api = new WebServiceConfig($config['OPTIONS']);
//$api->setSdkConfig($config);
//$api->setInputData($inputData)
//    ->createConfig();
//
//// api list
//$api = new WebServiceConfig($config['OPTIONS']);
//$api->setSdkConfig($config);
//$api->listConfig();
//
//api update
$api = new WebServiceConfig($config['OPTIONS']);
$api->setSdkConfig($config);
$api->setInputData($inputData)
    ->updateConfig();


echo "<pre>";
print_r($api->getResponse());
echo "</pre>";